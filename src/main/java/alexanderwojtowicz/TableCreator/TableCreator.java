package alexanderwojtowicz.TableCreator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Vector;

/**
 * This is the keyboard-based version of TableCreator. This driver allows
 * the user to create and manipulate tables at will.
 * 
 * @author Alexander Wojtowicz
 */

public class TableCreator {
//=============================================================================
	final static int COL_WIDTH = 80;
	final static String OPTION_SEPARATOR = "`";
	static InputStreamReader isr = new InputStreamReader(System.in);
	static BufferedReader reader = new BufferedReader(isr);
	static List<String> options = new Vector<String>();
	static List<Table> tables = new Vector<Table>();
//=============================================================================
	public static void main(String[] args) throws IOException {
		options.add("name");
		options.add("description");
		options.add("add");
		options.add("delete");
		options.add("load-txt");
		options.add("load-csv");
		options.add("load-cancel");
		options.add("save-txt");
		options.add("save-csv");
		options.add("save-cancel");
		options.add("switch-existing");
		options.add("switch-new");
		options.add("switch-cancel");
		options.add("exit");
		options.add("view");
		options.add("clear");
		options.add("view all");
		options.add("clear all");
		options.add("delete col");
		options.add("delete row");
		
		Table newTable = new Table(greeting("init"));
		tables.add(newTable);
		String toService = menu(tables.get(0));
		String currTable = findCurrTable(toService);
		String option = findOption(toService);
		//System.out.println(toService);
		//System.out.println(currTable);
		//System.out.println(option);
		currTable = service(currTable, option);
		while(!currTable.equals(OPTION_SEPARATOR)) {
			Table workingTable = new Table();
			boolean found = false;
			for(int i=0; i<tables.size(); i++) {
				if(tables.get(i).getName().equals(currTable)) {
					workingTable = tables.get(i);
					found = true;
					break;
				}
			}
			if(found) {
				System.out.println(workingTable.getName());
				toService = menu(workingTable);
				currTable = findCurrTable(toService);
				option = findOption(toService);
				currTable = service(currTable, option);
			}
			else {
				printHeading("FATAL ERROR: Table not found!",
						COL_WIDTH/2, 4);
				printLine("currTable String: " + currTable, COL_WIDTH/2);
				currTable = OPTION_SEPARATOR;
			}
		}
		printHeading("Good-Bye!", COL_WIDTH, 2);
		reader.close();
		isr.close();
	}
// METHODS ====================================================================	
//=============================================================================
	/*
	 * Finds the name of the current table given a String returned from menu().
	 */
	public static String findCurrTable(String toService) {
		String currTable = "";
		if(!toService.contains(OPTION_SEPARATOR)) {
			printHeading("ERROR: No option separator found!", COL_WIDTH/2, 4);
		}
		else {
			char[] arr = toService.toCharArray();
			int sIndex = 0;
			for(int i=0; i<arr.length; i++) {
				if(arr[i] == OPTION_SEPARATOR.toCharArray()[0]) {
					sIndex = (i + 1);
					break;
				}
			}
			for(int i=sIndex; i<arr.length; i++) {
				currTable = currTable + arr[i];
			}
		}
		
		return currTable;
	}
//=============================================================================
	/*
	 * Finds the name of the option given a String returned from the menu().
	 */
	public static String findOption(String toService) {
		String option = "";
		if(!toService.contains(OPTION_SEPARATOR)) {
			printHeading("ERROR: No option separator found!", COL_WIDTH/2, 4);
		}
		else {
			char[] arr = toService.toCharArray();
			for(int i=0; i<arr.length; i++) {
				if(arr[i] == OPTION_SEPARATOR.toCharArray()[0]) {
					break;
				}
				else {
					option = option + arr[i];
				}
			}
		}
		
		return option;
	}
//=============================================================================
	/*
	 * For outputting horizontal lines.
	 */
	public static void printHline(char character, int colWidth) {
		for(int i=0; i<colWidth; i++) {
			System.out.print(character);
		}
		System.out.println();
	}
//=============================================================================
	/*
	 * For outputting headings.
	 */
	public static void printHeading(String header, int colWidth, int choice) {
		// Setup...
		List<Character> charList = new Vector<Character>();
		charList.add('*'); // 0
		charList.add('-'); // 1
		charList.add('='); // 2
		charList.add('+'); // 3
		charList.add('#'); // 4
		charList.add('%'); // 5
		char divChar = '!';
		if((choice < 0) || (choice > (charList.size()-1))) {
			divChar = charList.get(0);
		}
		else {
			divChar = charList.get(choice);
		}
		
		// Printing...
		String divider = new String();
		for(int i=0; i<colWidth; i++) {
			divider = divider + divChar;
		}
		if(header.length() == colWidth) {
			System.out.println(divider);
			System.out.println(header);
			System.out.println(divider);
		}
		else if(header.length() < colWidth) {
			int totalBlanks = (colWidth - header.length());
			int leftBlanks = (totalBlanks / 2);
			int rightBlanks = (totalBlanks - leftBlanks);
			String lMargin = "";
			String rMargin = "";
			for(int i=0; i<leftBlanks; i++) {
				lMargin = lMargin + " ";
			}
			for(int i=0; i<rightBlanks; i++) {
				rMargin = rMargin + " ";
			}
			System.out.println(divider);
			System.out.println(lMargin + header + rMargin);
			System.out.println(divider);
		}
		else {
			System.out.println("ERROR -> THE HEADER IS TOO LONG!");
		}
	}
//=============================================================================
	/*
	 * TODO
	 * Print a string of characters within the colWidth with formatting.
	 */
	public static void printLine(String text, int colWidth) {
		System.out.println(text);
	}
//=============================================================================
	/*
	 * Print text with left and right sides. Connect with dots.
	 */
	public static void printOption(String left, String right, int colWidth) {
		final char separator = '.';
		left = left + " ";
		right = " " + right;
		if(left.length() + right.length() > colWidth) {
			System.out.println("ERROR: Cannot print option! Shorten lhs/rhs.");
		}
		else {
			int numDots = (colWidth - (left.length() + right.length()));
			System.out.print(left);
			for(int i=0; i<numDots; i++) {
				System.out.print(separator);
			}
			System.out.print(right + "\n");
		}
	}
//=============================================================================
	/**
	 * TODO
	 * A greeting message and generation of table name from user input.
	 * 
	 * @param init A String indicating if this is the first table.
	 * @return tName A String indicating the title of a table.
	 * @throws IOException 
	 */
	public static String greeting(String init) throws IOException {
		if(!init.equals("")) {
			printHeading("Welcome to TableCreator!", COL_WIDTH, 2);
		}
		boolean dupName = false;
		String tName = OPTION_SEPARATOR;
		while(tName.contains(OPTION_SEPARATOR)) {
			printLine("Give your table a name: ", COL_WIDTH);
			tName = getInput(reader);
			if(tName.contains(OPTION_SEPARATOR)) {
				printHeading(("ERROR: Cannot use '" + OPTION_SEPARATOR + "'" 
						+ " in name!"), COL_WIDTH/2, 4);
			}
			for(int i=0; i<tables.size(); i++) {
				if(tName.equals(tables.get(i).getName())) {
					tName = OPTION_SEPARATOR;
					dupName = true;
				}
			}
			if(dupName == true) {
				printHeading(("ERROR: Duplicate table name found!"), 
						COL_WIDTH/2, 4);
			}
			dupName = false;
		}
		
		return tName;
	}
//=============================================================================
	/*
	 * Get user input and return it.
	 */
	public static String getInput(BufferedReader reader) throws IOException {
		String userInput = "NULL-INPUT";
		userInput = reader.readLine();
		
		return userInput;
	}
//=============================================================================
	/**
	 * TODO
	 * Prompts the user for keyboard input.
	 * 
	 * @return choice A String
	 * @throws IOException 
	 */
	public static String menu(Table currTable) throws IOException {
		String choice = "null";
		
		while(!options.contains(choice)) {
			printHeading("Main Menu", COL_WIDTH, 0);
			printOption("WORKING TABLE", currTable.getName(), COL_WIDTH);
			System.out.println();
			printOption("'name'"       , "Change the name of current table"  , COL_WIDTH);
			printOption("'description'", "Add a description to current table", COL_WIDTH);
			printOption("'add'"        , "Add an entry to current table"     , COL_WIDTH);
			printOption("'delete'"     , "Delete an entry from current table", COL_WIDTH);
			printOption("'delete row"  , "Delete a row from current table"   , COL_WIDTH);
			printOption("'delete col"  , "Delete a column from current table", COL_WIDTH);
			printOption("'load'"       , "Load a table from a file"          , COL_WIDTH);
			printOption("'save'"       , "Save current table to file"        , COL_WIDTH);
			printOption("'switch'"     , "Switch to a new working table"     , COL_WIDTH);
			printOption("'view'"       , "View the current table"            , COL_WIDTH);
			printOption("'view all'"   , "View all tables"                   , COL_WIDTH);
			printOption("'clear'"      , "Discard the current table"         , COL_WIDTH);
			printOption("'clear all'"  , "Discard all tables"                , COL_WIDTH);
			printOption("'exit'"       , "Exit TableCreator"                 , COL_WIDTH);
			System.out.println();
			choice = getInput(reader).toLowerCase();
			printHline('-', COL_WIDTH);
			System.out.println();
		
			if(choice.equals("load")) {
				printLine("Are you loading from a .txt or .csv file?", COL_WIDTH);
				printOption("'txt'"   , "Select ASCII Text option"   , COL_WIDTH);
				printOption("'csv'"   , "Select CSV option"          , COL_WIDTH);
				printOption("'cancel'", "Return to the main menu"    , COL_WIDTH);
				choice = choice + "-" + getInput(reader);
				printHline('-', COL_WIDTH);
				System.out.println();
			}
			else if(choice.equals("save")) {
				printLine("Are you saving to a .txt or .csv file?", COL_WIDTH);
				printOption("'txt'"   , "Select ASCII Text option", COL_WIDTH);
				printOption("'csv'"   , "Select CSV option"       , COL_WIDTH);
				printOption("'cancel'", "Return to the main menu" , COL_WIDTH);
				choice = choice + "-" + getInput(reader);
				printHline('-', COL_WIDTH);
				System.out.println();
			}
			else if(choice.equals("switch")) {
				printLine("Are you switching to an existing or new table?", COL_WIDTH);
				printOption("'existing'", "Switch to an existing table"   , COL_WIDTH);
				printOption("'new'"     , "Switch to a new table"         , COL_WIDTH);
				printOption("'cancel'"  , "Return to the main menu"       , COL_WIDTH);
				choice = choice + "-" + getInput(reader);
				printHline('-', COL_WIDTH);
				System.out.println();
			}
			if(!options.contains(choice)) {
				printHeading("ERROR: Invalid choice!", COL_WIDTH/2, 4);
			}
		}
		
		return choice.toLowerCase() + OPTION_SEPARATOR + currTable.getName();
	}
//=============================================================================
	/**
	 * TODO
	 * Carries out the user's commands regarding table manipulation.
	 * 
	 * @param currTable A string corresponding to the current table
	 * @param option The desired table operation
	 * 
	 * @return tableName The name of the current working table (post operation)
	 * @throws IOException 
	 */
	public static String service(String currTable, String option) throws IOException {
		String tableName = "tableName -> ERROR!";
		if(option.contains("cancel")) {
			tableName = currTable;
		}
		else if(option.equals("name")) {
			tableName = greeting("");
			int cIndex = -1;
			for(int i=0; i<tables.size(); i++) {
				if(tables.get(i).getName().equals(currTable)) {
					tables.get(i).setName(tableName);
					cIndex = i;
					break;
				}
			}
			if(cIndex < 0) {
				printHeading("ERROR: Table '" + currTable + 
						"' not found in collection", COL_WIDTH, 4);
			}
		}
		else if(option.equals("description")) {
			
		}
		else if(option.equals("add")) {
			printLine("Column Name: ", COL_WIDTH);
			String col = getInput(reader);
			printLine("Row Name: ", COL_WIDTH);
			String row = getInput(reader);
			printLine("Value: ", COL_WIDTH);
			String val = getInput(reader);
			int indexOf = -1;
			for(int i=0; i<tables.size(); i++) {
				if(tables.get(i).getName().equals(currTable)) {
					indexOf = i;
					break;
				}
			}
			if(indexOf < 0) {
				printHeading("ERROR: Table not found!", COL_WIDTH, 4);
			}
			else {
				tableName = currTable;
				tables.get(indexOf).add(row, col, val);
			}
		}
		else if(option.equals("delete")) {
			
		}
		else if(option.equals("load-txt")) {
			
		}
		else if(option.equals("load-csv")) {
			
		}
		else if(option.equals("save-txt")) {
			
		}
		else if(option.equals("save-csv")) {
			
		}
		else if(option.equals("switch-existing")) {
			String toFind = OPTION_SEPARATOR;
			boolean found = false;
			while(toFind.equals(OPTION_SEPARATOR)) {
				printLine("Existing table name: ", COL_WIDTH);
				toFind = getInput(reader);
				for(int i=0; i<tables.size(); i++) {
					if(tables.get(i).getName().equals(toFind)) {
						found = true;
					}
				}
				if(!found) {
					printHeading("ERROR: '" + toFind + "' not found!"
							, COL_WIDTH/2, 4);
					toFind = OPTION_SEPARATOR;
				}
			}
			tableName = toFind;
		}
		else if(option.equals("switch-new")) {
			printHeading("Creating new table...", COL_WIDTH/2, 0);
			Table newTable = new Table();
			tableName = greeting("");
			newTable.setName(tableName);
			tables.add(newTable);
		}
		else if(option.equals("view")) {
			List<Table> thisTable = new Vector<Table>();
			for(int i=0; i<tables.size(); i++) {
				if(tables.get(i).getName().equals(currTable)) {
					thisTable.add(tables.get(i));
					break;
				}
			}
			if(thisTable.equals(new Vector<Table>())) {
				printHeading("ERROR: '" + currTable + "' could not be found!"
						, COL_WIDTH/2, 4);
			}
			else {
				viewTables(thisTable);
			}
			tableName = currTable;
		}
		else if(option.equals("view all")) {
			viewTables(tables);
			tableName = currTable;
		}
		else if(option.equals("clear")) {
			
		}
		else if(option.equals("clear all")) {
			
		}
		else if(option.equals("delete col")) {
			
		}
		else if(option.equals("delete row")) {
			
		}
		else if(option.equals("exit")) {
			tableName = OPTION_SEPARATOR;
		}
		else {
			printHeading("ERROR: Option was not recognized!", COL_WIDTH/2, 4);
		}
		
		return tableName;
	}
//=============================================================================
	/**
	 * View a collection of tables.
	 */
	public static void viewTables(List<Table> tables) {
		for(int i=0; i<tables.size(); i++) {
			printHeading(("TABLE: " + tables.get(i).getName()), COL_WIDTH, 5);
			tables.get(i).display('c');
			System.out.println();
		}
	}
//=============================================================================
} // End of TableCreator.java =================================================