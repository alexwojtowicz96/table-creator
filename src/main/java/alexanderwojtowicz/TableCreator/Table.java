package alexanderwojtowicz.TableCreator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.io.PrintWriter;

/**
 * This class contains tools for creating and outputting tables. The outputs
 * aim to be aesthetically pleasing. Data will may be aligned in the cells
 * left, right or center.
 * 
 * @author Alexander Wojtowicz
 */
public class Table {
	public final char EMPTY = ' ';      // Fills an empty cell.
	public final int WIDTH_LIMIT = 72;  // Max width for lines of columns.
//=============================================================================	
// Constructors ---------------------------------------------------------------
//=============================================================================
	/**
	 * The default constructor. Create an empty table with no row or columns.
	 */
	public Table() {
		table          = new HashMap<String, String>();
		colHeadings    = new Vector<String>();
		rowHeadings    = new Vector<String>();
		rowTitle       = new String();
		name           = new String();
		description    = new String();
		outDescription = new String();
	}
//=============================================================================
//=============================================================================
	/**
	 * Create a table with a name.
	 */
	public Table(String n) {
		table          = new HashMap<String, String>();
		colHeadings    = new Vector<String>();
		rowHeadings    = new Vector<String>();
		rowTitle       = new String();
		name           = n;
		description    = new String();
		outDescription = new String();
	}
// Methods --------------------------------------------------------------------
//=============================================================================
	/**
	 * For printing errors.
	 */
	public void printError(String message, String reason) {
		System.out.println("ERROR : " + message);
		System.out.println("-> " + reason);
	}
//=============================================================================
	/**
	 * Show the table with minimal formatting.
	 */
	public void describe() {
		String indent = "    ";
		Set<String> tableKeys = table.keySet();
		System.out.println("Entries:");
		for(String k : tableKeys) {
			System.out.println(indent + k + " : " + table.get(k));
		}
		System.out.println("\nColumns:");
		for(int i=0; i<colHeadings.size(); i++) {
			System.out.println(indent + "Column #" + i 
					+ " : " + colHeadings.get(i));
		}
		System.out.println("\nRows:");
		for(int i=0; i<rowHeadings.size(); i++) {
			System.out.println(indent + "Row #" + i
					+ " : " + rowHeadings.get(i));
		}
		System.out.println();
		
		System.out.println("*** VISUALIZATION ***");
		indent = new String();
		String longest = rowHeadings.get(0);
		for(int i=0; i<rowHeadings.size(); i++) {
			if(rowHeadings.get(i).length() > longest.length()) {
				longest = rowHeadings.get(i);
			}
		}
		longest = longest + " ";
		for(int l=0; l<longest.length(); l++) {
			System.out.print(" ");
		}
		for(int c=0; c<colHeadings.size(); c++) {
			System.out.print(colHeadings.get(c).toUpperCase() + " ");
		}
		System.out.println("\n");
		for(int i=0; i<rowHeadings.size(); i++) {
			String currRow = rowHeadings.get(i);
			System.out.print(currRow.toUpperCase());
			for(int l=currRow.length(); l<longest.length(); l++) {
				System.out.print(" ");
			}
			for(int j=0; j<colHeadings.size(); j++) {
				String currCol = colHeadings.get(j);
				if(!table.containsKey(makeEntry(currCol, currRow))) {
					System.out.print(EMPTY);
				}
				else {
					System.out.print(table.get(makeEntry(currCol, currRow)));
				}
				System.out.print(" ");
			}
			System.out.println();
		}
		System.out.println();
	}
//=============================================================================
	/*
	 * This is quick and dirty helper method to evaluate if an integer is odd.
	 * Return TRUE if odd, FALSE if even.
	 */
	public boolean isOdd(int integer) {
		int halved = (integer / 2);
		return (integer != (halved + halved));
	}
//=============================================================================
	/**
	 * Display the table to the screen. Alignment options are:
	 * 'l' -> Left
	 * 'r' -> Right
	 * 'c' -> Center
	 * 
	 * @param alignment The cell-text alignment option
	 */
	public void display(char alignment) {
		System.out.print(toString(alignment));
	}
//=============================================================================
	/**
	 * Format this.description. This creates this.outDescription.
	 */
	public void formatDescription() {
		this.outDescription = "";
		outDescription = outDescription + ("\nDESCRIPTION:\n");
		String bullet = "-> ";
		String indent = "   ";
		final int psudoWidth = WIDTH_LIMIT - bullet.length();
		if(this.description.length() <= psudoWidth) {
			outDescription = outDescription + (bullet + description + "\n");
		}
		else {
			List<String> words = new Vector<String>();
			char[] descArr = description.toCharArray();
			String currWord = "";
			for(int i=0; i<descArr.length; i++) {
				if(descArr[i] == ' ') {
					words.add(currWord);
					currWord = "";
				}
				else {
					currWord = currWord + descArr[i];
				}
			}
			if(!currWord.equals("")) {
				words.add(currWord);
				currWord = new String();
			}
			List<String> lines = new Vector<String>();
			String theLine = "";
			int charCounter = 0;
			for(int i=0; i<words.size(); i++) {
				currWord = words.get(i);
				if(charCounter + currWord.length() > psudoWidth) {
					lines.add(theLine);
					theLine = "";
					theLine = theLine + " " + currWord;
					charCounter = 0;
				}
				else {
					theLine = theLine + " " + currWord;
					charCounter = theLine.length();
				}
			}
			if(!theLine.equals("")) {
				lines.add(theLine);
			}
			for(int i=0; i<lines.size(); i++) {
				if(i == 0) {
					outDescription = outDescription + (bullet + lines.get(i) + "\n");
				}
				else {
					outDescription = outDescription + (indent + lines.get(i) + "\n");
				}
			}
		}
	}
//=============================================================================
	/**
	 * Save the table to an ASCII Text file. Alignment options are:
	 * 'l' -> Left
	 * 'r' -> Right
	 * 'c' -> Center
	 * 
	 * @param outFile The relative or absolute path to the file
	 * @param alignment The cell-text alignment option
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void save(File outFile, char alignment) 
			throws FileNotFoundException, IOException {
		char[] path = outFile.toString().toCharArray();
		String ext = "";
		for(int i=4; i>0; i--) {
			ext = ext + path[(path.length) - i];
		}
		if(!ext.equals(".txt")) {
			printError("The table could not be exported to file",
					"Is your file extension '.txt'?");
		}
		else {
			PrintWriter writer = new PrintWriter(outFile.getCanonicalFile());
			writer.print(toString(alignment));
			writer.close();
			System.out.println("-> Saved to: " + outFile.getCanonicalPath());
		}
	}
//=============================================================================
	/**
	 * Create an entry name for the table hash map. This is not adding anything
	 * to this.table!
	 * 
	 * @param colName
	 * @param rowName
	 * @return entry A String
	 */
	public String makeEntry(String colName, String rowName) {
		return (colName + "-" + rowName);
	}
//=============================================================================
	/**
	 * Add an item to the table. If the row doesn't exists, make a new row. 
	 * If the column doesn't exist, make a new column.
	 * 
	 * @param rowName The name of the row corresponding to the value
	 * @param colName The name of the column corresponding to the value
	 * @param value the value to be put at intersection of rowName:colName
	 */
	public void add(String rowName, String colName, String value) {
		if((colName.equals("") || rowName.equals("")) && (!value.equals(""))) {
			printError("Cannot add value to null column/row name!",
					"Illegal operation.");
		}
		else {
			int option = 0;
			if(colName.equals("")) {
				option = 1;
			}
			if(rowName.equals("")) {
				option = 2;
			}
			if(rowName.equals("") && colName.equals("")) {
				option = 3;
			}
			// Column and Row names DNE...
			if(!(colHeadings.contains(colName)) 
					&& !(rowHeadings.contains(rowName))) {
				if(option == 0) {
					colHeadings.add(colName);
					rowHeadings.add(rowName);
					table.put(makeEntry(colName, rowName), value);
				}
				else if(option == 1) {
					rowHeadings.add(rowName);
				}
				else if(option == 2) {
					colHeadings.add(colName);
				}
				else {
					// Do nothing...
				}
			}
			// Column name DNE...
			else if(!(colHeadings.contains(colName)) 
					&& (rowHeadings.contains(rowName))) {
				if(option == 0) {
					colHeadings.add(colName);
					table.put(makeEntry(colName, rowName), value);
				}
				else if(option == 1) {
					// Do nothing...
				}
				else if(option == 2) {
					colHeadings.add(colName);
				}
				else {
					// Do nothing...
				}
			}
			// Row name DNE...
			else if((colHeadings.contains(colName)) 
					&& !(rowHeadings.contains(rowName))) {
				if(option == 0) {
					rowHeadings.add(rowName);
					table.put(makeEntry(colName, rowName), value);
				}
				else if(option == 1) {
					rowHeadings.add(rowName);
				}
				else if(option == 2) {
					// Do nothing...
				}
				else { 
					// Do nothing...
				}
			}
			// Both Row and Column names exist...
			else if((colHeadings.contains(colName)) 
					&& (rowHeadings.contains(rowName))) {
				if(option == 0) {
					table.put(makeEntry(colName, rowName), value);
				}
				else if(option == 1) {
					// Do nothing...
				}
				else if(option == 2) {
					// Do nothing...
				}
				else {
					// Do nothing...
				}
			}
			else {
				printError("Could not add entry to table.", "Unknown cause.");
			}
		}
	}
//=============================================================================
	/**
	 * TODO
	 * Delete an item in a particular cell at rowName:colName intersection.
	 * 
	 * @param rowName The name of the row corresponding to the value
	 * @param colName The name of the column corresponding to the value
	 * @param value The value to be deleted at intersection of rowName:colName
	 */
	public void removeEntry(String colName, String rowName, String value) {
		
	}
//=============================================================================
	/**
	 * TODO
	 * Remove an entire row from the table
	 * 
	 * @param rowName The name of the row to delete
	 */
	public void removeRow(String rowName) {
		
	}
//=============================================================================
	/**
	 * TODO
	 * Remove an entire column from the table
	 * 
	 * @param colName The name of the column to delete
	 */
	public void removeCol(String colName) {
		
	}
//=============================================================================
	/**
	 * Remove everything in the table.
	 */
	public void clean() {
		table = new HashMap<String, String>();
		// Add empty chars to cells with row/col headings...
	}
//=============================================================================
	/**
	 * Reset the {@link Table} object back to defaults.
	 */
	public void reset() {
		table          = new HashMap<String, String>();
		colHeadings    = new Vector<String>();
		rowHeadings    = new Vector<String>();
		rowTitle       = new String();
		name           = new String();
		description    = new String();
		outDescription = new String();
	}
//=============================================================================
	/**
	 * Get the entire {@link Table}.
	 * 
	 * @return table A List<HashMap<String, String>>
	 */
	public HashMap<String, String> getTable() {
		return this.table;
	}
//=============================================================================
	/**
	 * Get the current list of row headings.
	 * 
	 * @return rowHeadings A List<String>
	 */
	public List<String> getRowHeadings() {
		return this.rowHeadings;
	}
//=============================================================================
	/**
	 * TODO
	 * Load a table file (ASCII Text) into a {@link Table}.
	 * 
	 * @param filePath Absolute or Relative path to .txt file
	 * @throws IOException 
	 */
	public void load(File filePath) throws IOException {
		if(!filePath.getCanonicalFile().exists()) {
			printError("The file could not be found.",
					"Are you sure the location is "
					+ filePath.getCanonicalPath() + " ?");
		}
		else {
			char[] path = filePath.toString().toCharArray();
			String ext = "";
			for(int i=4; i>0; i--) {
				ext = ext + path[(path.length) - i];
			}
			if(!ext.equals(".txt")) {
				printError("The file type is unrecognized.",
						"Is your file extension '.txt'?");
			}
			else {
				reset();
				Scanner scanner = new Scanner(filePath);
				char[] nameArr = scanner.nextLine().toCharArray();
				List<String> tempList = new Vector<String>();
				List<String> descData = new Vector<String>();
				String currWord = "";
				for(int i=0; i<nameArr.length; i++) {
					if(nameArr[i] != ' ') {
						currWord = currWord + nameArr[i];
					}
					else {
						if(!currWord.equals("")) {
							tempList.add(currWord);
							currWord = "";
						}
					}
				}
				if(!currWord.equals("")) {
					tempList.add(currWord);
				}
				for(int i=0; i<tempList.size(); i++) {
					this.name = name + tempList.get(i);
					if(i != (tempList.size()-1)) {
						this.name = name + " ";
					}
				}
				scanner.nextLine();
				String divider = scanner.nextLine();
				char[] fieldData = scanner.nextLine().toCharArray();
				tempList = new Vector<String>();
				currWord = "";
				for(int i=0; i<fieldData.length; i++) {
					if(fieldData[i] == ' ') {
						if(!currWord.equals("")) {
							tempList.add(currWord);
							currWord = "";
						}
					}
					else {
						currWord = currWord + fieldData[i];
					}
				}
				if(!currWord.equals("")) {
					tempList.add(currWord);
				}
				List<Integer> divIndexes = new Vector<Integer>(); 
				for(int i=0; i<tempList.size(); i++) {
					if(tempList.get(i).equals("|")) {
						divIndexes.add(i);
					}
				}
				boolean hasFailed = false;
				String msg = "Could not load table.";
				String reason = "";
				for(int i=0; i<(divIndexes.size()-1); i++) {
					int currIndex = divIndexes.get(i);
					int nextIndex = divIndexes.get(i+1);
					if(nextIndex == (currIndex + 1)) {
						if(currIndex != 0) {
							hasFailed = true;
							reason = "NULL field names are invalid!";
							break;
						}
						else {
							this.rowTitle = "" + EMPTY;
						}
					}
					if(currIndex == 0) {
						if(nextIndex != 1) {
							for(int j=(0+1); j<(nextIndex); j++) {
								this.rowTitle = rowTitle + tempList.get(j) + " ";
							}
							char[] t = this.rowTitle.toCharArray();
							if(t[(t.length)-1] == ' ') {
								this.rowTitle = new String();
								for(int j=0; j<(t.length-1); j++) {
									this.rowTitle = rowTitle + t[j];
								}
							}
						}
					}
					else {
						String entry = new String();
						for(int j=(currIndex+1); j<(nextIndex); j++) {
							entry = entry + tempList.get(j) + " ";
						}
						char[] t = entry.toCharArray();
						if(t[(t.length)-1] == ' ') {
							entry = new String();
							for(int j=0; j<(t.length-1); j++) {
								entry = entry + t[j];
							}
						}
						this.colHeadings.add(entry);
					}
				}
				if(hasFailed) {
					printError(msg, reason);
				}
				else {
					boolean inDesc = false;
					while(scanner.hasNextLine()) {
						String currLine = scanner.nextLine();
						if(currLine.contains("DESCRIPTION:")) {
							inDesc = true;
							//break;
						}
						else {
							if(inDesc == false) {
								if(!currLine.contains(divider)) {
									tempList = new Vector<String>();
									currWord = "";
									char[] line = currLine.toCharArray();
									for(int i=0; i<line.length; i++) {
										if(line[i] == ' ') {
											if(!currWord.equals("")) {
												tempList.add(currWord);
												currWord = "";
											}
										}
										else {
											currWord = currWord + line[i];
										}
									}
									if(!currWord.equals("")) {
										tempList.add(currWord);
									}
									List<String> tuple = new Vector<String>();
									currWord = "";
									for(int i=0; i<tempList.size(); i++) {
										if(tempList.get(i).equals("|")) {
											if(!currWord.equals("")) {
												tuple.add(currWord.trim());
												currWord = "";
											}
											else {
												tuple.add("" + EMPTY);
											}
										}
										else {
											currWord = currWord + tempList.get(i) + " ";
										}
									}
									if(!tuple.equals(new Vector<String>())) {
										tuple.remove(0);
										for(int i=0; i<this.colHeadings.size(); i++) {
											add(tuple.get(0), colHeadings.get(i), tuple.get(i+1));
										}
									}
								}
							}
							else {
								descData.add(currLine);
							}
						}
					}
				}
				scanner.close();
				for(int i=0; i<descData.size(); i++) {
					char[] dLine = descData.get(i).toCharArray();
					String toAdd = "";
					for(int j=4; j<dLine.length; j++) {
						toAdd = toAdd + dLine[j];
					}
					toAdd = toAdd + " ";
					this.description = description + toAdd;
					//System.out.println(descData.get(i));
					//System.out.println("**toAdd: " + toAdd);
				}
			}
		}
	}
//=============================================================================
	/**
	 * Given a CSV file, load the table data into a {@link Table}
	 * 
	 * @param filePath Absolute or Relative path to .csv file
	 * @throws IOException 
	 */
	public void loadFromCsv(File filePath) throws IOException {
		if(!filePath.getCanonicalFile().exists()) {
			printError("The file could not be found.",
					"Are you sure the location is "
					+ filePath.getCanonicalPath() + " ?");
		}
		else {
			char[] path = filePath.toString().toCharArray();
			String ext = "";
			for(int i=4; i>0; i--) {
				ext = ext + path[(path.length) - i];
			}
			if(!ext.equals(".csv")) {
				printError("The file type is unrecognized.",
						"Is your file extension '.csv'?");
			}
			else {
				reset();
				int lineCounter = 0;
				Scanner scanner = new Scanner(filePath);
				boolean hasError = false;
				String msg = "Failed to create table from CSV file.";
				String reason = new String();
				while(scanner.hasNextLine()) {
					char[] currRow = scanner.nextLine().toCharArray();
					if(lineCounter == 0) {
						if(currRow[currRow.length-1] == ',') {
							hasError = true;
							reason = "The last field name is NULL.";
							break;
						}
						else {
							boolean hasFailed = false;
							for(int i=0; i<(currRow.length-1); i++) {
								if(currRow[i] == ',' && currRow[i+1] == ',') {
									hasError = true;
									reason = "Cannot have NULL field names!";
									hasFailed = true;
									break;
								}
							}
							if(hasFailed) {
								break;
							}
						}
					}
					String currWord = "";
					List<String> rowList = new Vector<String>();
					for(int i=0; i<currRow.length; i++) {
						if(currRow[i] == ',') {
							rowList.add(currWord);
							currWord = "";
						}
						else {
							currWord = currWord + currRow[i];
						}
					}
					if(!currWord.equals("")) {
						rowList.add(currWord);
					}
					//System.out.println(rowList);
					if(lineCounter == 0) {
						boolean hasFailed = false;
						for(int i=0; i<rowList.size(); i++) {
							String toCompare = rowList.get(i);
							for(int j=0; j<rowList.size(); j++) {
								if(rowList.get(j).equals(toCompare) && (i != j)) {
									hasError = true;
									reason = "Cannot have duplicate field names!";
									hasFailed = true;
									break;
								}
							}
							if(hasFailed) {
								break;
							}
						}
						if(hasFailed) {
							break;
						}
						rowTitle = rowList.get(0);
						for(int i=1; i<rowList.size(); i++) {
							if(!rowList.get(i).equals("")) {
								colHeadings.add(rowList.get(i));
							}
							else {
								String e = "" + EMPTY;
								colHeadings.add(e);
							}
						}
					}
					else {
						rowHeadings.add(rowList.get(0));
						for(int i=1; i<rowList.size(); i++) {
							table.put(makeEntry(colHeadings.get(i-1), 
									rowList.get(0)), rowList.get(i));
						}
					}
					lineCounter++;
				}
				if(hasError) {
					printError(msg, reason);
				}
				scanner.close();
			}
		}	
	}
//=============================================================================
	public String toString(char alignment) {
		String tableString = new String();
		boolean recognized = false;
		if(alignment == 'l') {
			recognized = true;
		}
		if(alignment == 'r') {
			recognized = true;
		}
		if(alignment == 'c') {
			recognized = true;
		}
		if(!recognized) {
			String error = "'" + alignment + "'" + " was not an option!";
			printError("Unrecognized option for cell-text alignment.", error);
		}
		else {
			HashMap<String, String> tCopy = new HashMap<String, String>();
			HashMap<String, Integer> spaceMatrix = new HashMap<String, Integer>();
			List<Vector<String>> columns = new Vector<Vector<String>>();
			Vector<String> currCol = new Vector<String>();
			if(!rowTitle.equals(new String())) {
				currCol.add(" " + rowTitle + " ");
			}
			else {
				currCol.add(" " + EMPTY + " ");
			}
			for(int i=0; i<rowHeadings.size(); i++) {
				currCol.add(" " + rowHeadings.get(i) + " ");
			}
			columns.add(currCol);
			currCol = new Vector<String>();
			for(int i=0; i<colHeadings.size(); i++) {
				currCol.add(" " + colHeadings.get(i) + " ");
				for(int j=0; j<rowHeadings.size(); j++) {
					if(table.containsKey(makeEntry
							(colHeadings.get(i), rowHeadings.get(j)))) {
						currCol.add(" " + (table.get(makeEntry
								(colHeadings.get(i), rowHeadings.get(j)))) + " ");
					}
					else {
						currCol.add(" " + EMPTY + " ");
					}	
				}
				columns.add(currCol);
				currCol = new Vector<String>();
			}
			for(int i=0; i<columns.size(); i++) {
				currCol = columns.get(i);
				for(int j=0; j<currCol.size(); j++) {
					tCopy.put(makeEntry(currCol.get(0), columns.get(0).get(j)), 
							currCol.get(j));
				}
			}
			for(int i=0; i<columns.size(); i++) {
				currCol = columns.get(i);
				String longest = currCol.get(0);
				for(int j=0; j<currCol.size(); j++) {
					if(currCol.get(j).length() > longest.length()) {
						longest = currCol.get(j);
					}
				}
				for(int j=0; j<currCol.size(); j++) {
					spaceMatrix.put(makeEntry(currCol.get(0), 
							columns.get(0).get(j)), 
							(longest.length() - currCol.get(j).length()));
				}
			}
			String emp = (" " + EMPTY + " ");
			if(!rowTitle.equals(new String())) {
				emp = (" " + rowTitle + " ");
			}
			// Create the divider...
			String divider = new String();
			divider = divider + "+";
			for(int i=0; i<tCopy.get(makeEntry(emp, emp)).length(); i++) {
				divider = divider + "-";
			}
			for(int i=0; i<spaceMatrix.get(makeEntry(emp, emp)); i++) {
				divider = divider + "-";
			}
			divider = divider + "+";
			for(int i=0; i<colHeadings.size(); i++) {
				String currW = (" " + colHeadings.get(i) + " ");
				for(int j=0; j<tCopy.get(makeEntry(currW, emp)).length(); j++) {
					divider = divider + "-";
				}
				for(int j=0; j<spaceMatrix.get(makeEntry(currW, emp)); j++) {
					divider = divider + "-";
				}
				divider = divider + "+";
			}
			// Display the name...
			if(this.name.length() >= divider.length()) {
				tableString = tableString + name.toUpperCase() + "\n";
			}
			else {
				int difference = (divider.length() - this.name.length());
				int lMargin = difference / 2;
				int rMargin = difference - lMargin;
				for(int i=0; i<lMargin; i++) {
					tableString = tableString + " ";
				}
				tableString = tableString + name.toUpperCase();
				for(int i=0; i<rMargin; i++) {
					tableString = tableString + " ";
				}
				tableString = tableString + "\n";
			}
			tableString = tableString + "\n";
			// Set the alignment option...
			if(alignment == 'l') {
				tableString = tableString + divider + "\n";
				tableString = tableString + "|";
				tableString = tableString + 
						(tCopy.get(makeEntry(emp, emp)).toUpperCase());
				for(int i=0; i<spaceMatrix.get(makeEntry(emp, emp)); i++) {
					tableString = tableString + " ";
				}
				tableString = tableString + "|";
				for(int i=0; i<colHeadings.size(); i++) {
					String currW = (" " + colHeadings.get(i) + " ");
					tableString = tableString + 
							(tCopy.get(makeEntry(currW, emp)).toUpperCase());
					for(int j=0; j<spaceMatrix.get(makeEntry(currW, emp)); j++) {
						tableString = tableString + " ";
					}
					tableString = tableString + "|";
				}
				tableString = tableString + "\n";
				tableString = tableString + divider + "\n";
				for(int i=0; i<rowHeadings.size(); i++) {
					String rowN = (" " + rowHeadings.get(i) + " ");
					tableString = tableString + "|";
					tableString = tableString + 
							(tCopy.get(makeEntry(emp, rowN)).toUpperCase());
					for(int j=0; j<spaceMatrix.get(makeEntry(emp, rowN)); j++) {
						tableString = tableString + " ";
					}
					tableString = tableString + "|";
					for(int j=0; j<colHeadings.size(); j++) {
						String colN = (" " + colHeadings.get(j) + " ");
						tableString = tableString + (tCopy.get(makeEntry(colN, rowN)));
						for(int k=0; k<spaceMatrix.get(makeEntry(colN, rowN)); k++) {
							tableString = tableString + " ";
						}
						tableString = tableString + "|";
					}
					tableString = tableString + "\n";
					tableString = tableString + divider + "\n";
				}
			}
			else if(alignment == 'r') {
				tableString = tableString + divider + "\n";
				tableString = tableString + "|";
				for(int i=0; i<spaceMatrix.get(makeEntry(emp, emp)); i++) {
					tableString = tableString + " ";
				}
				tableString = tableString + 
						(tCopy.get(makeEntry(emp, emp)).toUpperCase());
				tableString = tableString + "|";
				for(int i=0; i<colHeadings.size(); i++) {
					String currW = (" " + colHeadings.get(i) + " ");
					for(int j=0; j<spaceMatrix.get(makeEntry(currW, emp)); j++) {
						tableString = tableString + " ";
					}
					tableString = tableString + 
							(tCopy.get(makeEntry(currW, emp)).toUpperCase());
					tableString = tableString + "|";
				}
				tableString = tableString + "\n";
				tableString = tableString + divider + "\n";
				for(int i=0; i<rowHeadings.size(); i++) {
					String rowN = (" " + rowHeadings.get(i) + " ");
					tableString = tableString + "|";
					for(int j=0; j<spaceMatrix.get(makeEntry(emp, rowN)); j++) {
						tableString = tableString + " ";
					}
					tableString = tableString + 
							(tCopy.get(makeEntry(emp, rowN)).toUpperCase());
					tableString = tableString + "|";
					for(int j=0; j<colHeadings.size(); j++) {
						String colN = (" " + colHeadings.get(j) + " ");
						for(int k=0; k<spaceMatrix.get(makeEntry(colN, rowN)); k++) {
							tableString = tableString + " ";
						}
						tableString = tableString + 
								(tCopy.get(makeEntry(colN, rowN)));
						tableString = tableString + "|";
					}
					tableString = tableString + "\n";
					tableString = tableString + divider + "\n";
				}
			}
			else if(alignment == 'c') {
				int rMargin = 0;
				int lMargin = 0;
				tableString = tableString + divider + "\n";
				tableString = tableString + "|";
				if(isOdd(spaceMatrix.get(makeEntry(emp, emp)))) {
					lMargin = (spaceMatrix.get(makeEntry(emp, emp)) - 1) / 2;
					rMargin = lMargin + 1;
				}
				else {
					lMargin = spaceMatrix.get(makeEntry(emp, emp)) / 2;
					rMargin = lMargin;
				}
				for(int i=0; i<lMargin; i++) {
					tableString = tableString + " ";
				}
				tableString = tableString + 
						(tCopy.get(makeEntry(emp, emp)).toUpperCase());
				for(int i=0; i<rMargin; i++) {
					tableString = tableString + " ";
				}
				tableString = tableString + "|";
				for(int i=0; i<colHeadings.size(); i++) {
					String currW = (" " + colHeadings.get(i) + " ");
					if(isOdd(spaceMatrix.get(makeEntry(currW, emp)))) {
						lMargin = (spaceMatrix.get(makeEntry(currW, emp)) - 1) / 2;
						rMargin = lMargin + 1;
					}
					else {
						lMargin = spaceMatrix.get(makeEntry(currW, emp)) / 2;
						rMargin = lMargin;
					}
					for(int j=0; j<lMargin; j++) {
						tableString = tableString + " ";
					}
					tableString = tableString + 
							(tCopy.get(makeEntry(currW, emp)).toUpperCase());
					for(int j=0; j<rMargin; j++) {
						tableString = tableString + " ";
					}
					tableString = tableString + "|";
				}
				tableString = tableString + "\n";
				tableString = tableString + divider + "\n";
				for(int i=0; i<rowHeadings.size(); i++) {
					String rowN = (" " + rowHeadings.get(i) + " ");
					tableString = tableString + "|";
					if(isOdd(spaceMatrix.get(makeEntry(emp, rowN)))) {
						lMargin = (spaceMatrix.get(makeEntry(emp, rowN)) - 1) / 2;
						rMargin = lMargin + 1;
					}
					else {
						lMargin = spaceMatrix.get(makeEntry(emp, rowN)) / 2;
						rMargin = lMargin;
					}
					for(int j=0; j<lMargin; j++) {
						tableString = tableString + " ";
					}
					tableString = tableString + 
							(tCopy.get(makeEntry(emp, rowN)).toUpperCase());
					for(int j=0; j<rMargin; j++) {
						tableString = tableString + " ";
					}
					tableString = tableString + "|";
					for(int j=0; j<colHeadings.size(); j++) {
						String colN = (" " + colHeadings.get(j) + " ");
						if(isOdd(spaceMatrix.get(makeEntry(colN, rowN)))) {
							lMargin = (spaceMatrix.get(makeEntry(colN, rowN)) - 1) / 2;
							rMargin = lMargin + 1;
						}
						else {
							lMargin = spaceMatrix.get(makeEntry(colN, rowN)) / 2;
							rMargin = lMargin;
						}
						for(int k=0; k<lMargin; k++) {
							tableString = tableString + " ";
						}
						tableString = tableString + (tCopy.get(makeEntry(colN, rowN)));
						for(int k=0; k<rMargin; k++) {
							tableString = tableString + " ";
						}
						tableString = tableString + "|";
					}
					tableString = tableString + "\n";
					tableString = tableString + divider + "\n";
				}
			}
			// Display the description...
			formatDescription();
			tableString = tableString + outDescription;
		}
		
		return tableString;
	}
//=============================================================================
	/**
	 * Get the current list of column headings.
	 * 
	 * @return colHeadings A List<String>
	 */
	public List<String> getColHeadings() {
		return this.colHeadings;
	}
//=============================================================================
	/**
	 * Get the name of the {@link Table}.
	 * 
	 * @return name A String
	 */
	public String getName() {
		return this.name;
	}
//=============================================================================
	/**
	 * Get the description of the {@link Table}.
	 * 
	 * @return description A String
	 */
	public String getDescription() {
		return this.description;
	}
//=============================================================================
	/**
	 * Get this.outDescription.
	 * 
	 * @return outDescription A String
	 */
	public String getOutDescription() {
		return this.outDescription;
	}
//=============================================================================
	/**
	 * @return rowTitle The title of row names column.
	 */
	public String getRowTitle() {
		return this.rowTitle;
	}
//=============================================================================
	/**
	 * TODO
	 * Organize the contents of a table. This includes shifting the order of
	 * selected cells and ordering data fields in a specified order (i.e.
	 * alphanumeric descending/ascending).
	 * 
	 * @param TODO
	 */
	public void organize() {
		
	}
//=============================================================================
	/**
	 * Export the table to CSV.
	 * 
	 * @param filePath A new file (.csv format)
	 * @throws FileNotFoundException 
	 */
	public void saveToCsv(File filePath) throws FileNotFoundException {
		char[] path = filePath.toString().toCharArray();
		String ext = "";
		for(int i=4; i>0; i--) {
			ext = ext + path[(path.length) - i];
		}
		if(!ext.equals(".csv")) {
			printError("The file type is unrecognized.",
					"Is your file extension '.csv'?");
		}
		else {
			PrintWriter writer = new PrintWriter(filePath);
			String comma = ",";
			writer.print(rowTitle + comma);
			for(int i=0; i<colHeadings.size(); i++) {
				writer.print(colHeadings.get(i));
				if(i != (colHeadings.size()-1)) {
					writer.print(comma);
				}
			}
			writer.println();
			for(int i=0; i<rowHeadings.size(); i++) {
				writer.print(rowHeadings.get(i) + comma);
				for(int j=0; j<colHeadings.size(); j++) {
					if(!table.containsKey(makeEntry(
							colHeadings.get(j), rowHeadings.get(i)))) {
						writer.print("");
					}
					else {
						writer.print(table.get(makeEntry
							(colHeadings.get(j), rowHeadings.get(i))));
					}
					if(j != (colHeadings.size()-1)) {
						writer.print(comma);
					}
				}
				writer.println();
			}
			writer.close();
		}
	}
//=============================================================================
	/**
	 * TODO
	 * Make a deep copy of a {@link Table}.
	 */
	public Table clone() {	
		return this;
	}
//=============================================================================
	/**
	 * TODO
	 * Check if the table is empty.
	 * 
	 * @return isEmpty If the table is empty return true, if not, return false.
	 */
	public boolean isEmpty() {
		return false;
	}
//=============================================================================
	/**
	 * Set this.name
	 * 
	 * @param theName
	 */
	public void setName(String theName) {
		this.name = theName;
	}
//=============================================================================
	/**
	 * Set this.description
	 * 
	 * @param theDescription
	 */
	public void setDescription(String theDescription) {
		this.description = theDescription;
	}
//=============================================================================
	/**
	 * Set this.outDescription
	 * 
	 * @param newOutDesc
	 */
	public void setOutDescription(String newOutDesc) {
		this.outDescription = newOutDesc;
	}
//=============================================================================
	/**
	 * Set this.table
	 * 
	 * @param theTable
	 */
	public void setTable(HashMap<String, String> theTable) {
		this.table = theTable;
	}
//=============================================================================	
	/**
	 * Set this.rowTitle
	 * 
	 * @param theTitle
	 */
	public void setRowTitle(String theTitle) {
		this.rowTitle = theTitle;
	}
//=============================================================================	
	/**
	 * Set this.colHeadings
	 * 
	 * @param newColHeads
	 */
	public void setColHeadings(List<String> newColHeads) {
		this.colHeadings = newColHeads;
	}
//=============================================================================	
	/**
	 * Set this.rowHeadings
	 * 
	 * @param newRowHeads
	 */
	public void setRowHeadings(List<String> newRowHeads) {
		this.rowHeadings = newRowHeads;
	}
//=============================================================================	
// Private Members ------------------------------------------------------------
//=============================================================================
	private HashMap<String, String> table;
	private List<String> colHeadings;
	private List<String> rowHeadings;
	private String rowTitle;
	private String name;
	private String description;
	private String outDescription; // The description, formatted for output.
//=============================================================================	
} // End of Table.java --------------------------------------------------------