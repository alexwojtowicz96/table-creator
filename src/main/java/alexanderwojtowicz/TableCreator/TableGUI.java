package alexanderwojtowicz.TableCreator;

import java.awt.*;
import java.awt.event.*;
//import java.util.Vector;
import javax.swing.*;

public class TableGUI extends JFrame {
//=============================================================================
	static Table genericTable = new Table();
//=============================================================================
	public static void main(String[] args) {
		new TableGUI().setVisible(true);
	}
//=============================================================================
	private class TableWorker implements Runnable {
		public TableWorker() {
			genericTable.setName("Generic Table");
			genericTable.add("", "couches", "");
			genericTable.add("", "dishes", "");
			genericTable.add("", "cell-phones", "");
			genericTable.add("", "cream cheese", "");
			genericTable.add("Jimmy", "", "");
			genericTable.add("Sal", "", "");
			genericTable.add("Joe-bert", "", "");
			genericTable.add("Teej", "", "");
			genericTable.add("Bill", "", "");
			genericTable.add("Jimmy", "couches", "5");
			genericTable.add("Jimmy", "dishes", "166");
			genericTable.add("Jimmy", "cream cheese", "Of_course!" );
			genericTable.add("Sal", "couches", "300");
			genericTable.add("Sal", "cell-phones", "888,120,499");
			genericTable.add("Sal", "cream cheese", "I'm broke.!");
			genericTable.add("Joe-bert", "dishes", "200000000");
			genericTable.add("Teej", "cream cheese", "What?");
			genericTable.add("Bill", "couches", "No_furniture");
			genericTable.add("Bill", "cream cheese", "Yup.");
			genericTable.add("", "New-Column", "");
			genericTable.setDescription("This is a description!");
		}
		
		public void run() {
			logArea.setText(genericTable.toString('c'));
		}
	}
//=============================================================================
// PRIVATE MEMBERS ============================================================
	private JPanel inputPanel;
	private JPanel summaryPanel;
	//private Vector<Vector<JTextField>> cells;
	private JTextField field;
	private JTextArea logArea;
	private JButton submit;
	private TableWorker worker;
	//private Table genericTable;
//=============================================================================	
// CONSTRUCTOR ================================================================
	public TableGUI() {	
		super("Table Creator");
		setLocation(50, 75);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Container cp = getContentPane();
		inputPanel   = new JPanel();
		summaryPanel = new JPanel();
		field        = new JTextField(20);
		submit       = new JButton("Submit");
		logArea      = new JTextArea("", 40, 40);
		field.setEnabled(true);
		logArea.setEditable(false);
		Font font = new Font("Courier", Font.PLAIN, 16);
		logArea.setFont(font);
		logArea.setBackground(Color.BLACK);
		logArea.setForeground(Color.WHITE);
		JScrollPane logPane = new JScrollPane(
			logArea,
			JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS
		);
		inputPanel.setLayout(new FlowLayout());
		summaryPanel.setLayout(new FlowLayout());
		inputPanel.add(field);
		inputPanel.add(submit);
		
		submit.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					worker = new TableWorker();
					//logArea.setText("submitted!");
					//logArea.setText(new String());
					//field.setText(new String());
					new Thread(worker).start();
				}
			}
		);
		cp.setLayout(new BorderLayout());
		cp.add(inputPanel, BorderLayout.NORTH);
		cp.add(summaryPanel, BorderLayout.SOUTH);
		cp.add(logPane, BorderLayout.CENTER);
		
		pack();
		submit.setEnabled(true);
	}
//=============================================================================
} // End of TableGUI.java =====================================================