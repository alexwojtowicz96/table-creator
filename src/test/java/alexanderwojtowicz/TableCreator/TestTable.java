package alexanderwojtowicz.TableCreator;

import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

/**
 * This class tests Table.java
 * 
 * @author Alexander Wojtowicz
 */
public class TestTable {
// Setup ----------------------------------------------------------------------
//=============================================================================
	final int COL_WIDTH = 150;      // Width of terminal.
	final char SEPARATOR = '*';     // Separator for tests.
	String resDir;                  // Testing resources directory.
	Table testTable;                // A sample Table object.
	Table table1;                   // A sample Table object.
	File file1;                     // A test file to use.
	File file2;                     // A test file to use.
	File file3;                     // CSV file to load from.
	File file4;                     // A test file to use.
	File file5;                     // Test writing to CSV file with this.
	File file6;                     // A test file to use.
	String desc;                    // A description for the table.
//=============================================================================
	@Before
	public void setup() {
		String classLoc = TestTable.class.getProtectionDomain().
				getCodeSource().getLocation().getPath().toString();
		String resources = "../../../../src/test/resources/";
		resDir = classLoc + resources;
		
		testTable = new Table();
		table1 = new Table();
		file1 = new File(resDir + "testTable.txt");
		file2 = new File(resDir + "outputCSV.txt");
		file3 = new File(resDir + "testCsv.csv");
		file4 = new File(resDir + "testTable2.txt");
		file5 = new File(resDir + "testSaveToCsv().csv");
		file6 = new File(resDir + "testTable-diff.txt");
		desc = ("this is a very long description. most of it is"
				+ " complete nonsense, but that is okay since this is a test."
				+ " I am not sure how this will turn out, but hopefully this"
				+ " will be displayed properly on seperate lines since this is"
				+ " obviously more than 72 charaters wide. This better work!");
		table1.add("", "couches", "");
		table1.add("", "dishes", "");
		table1.add("", "cell-phones", "");
		table1.add("", "cream cheese", "");
		table1.add("Jimmy", "", "");
		table1.add("Sal", "", "");
		table1.add("Joe-bert", "", "");
		table1.add("Teej", "", "");
		table1.add("Bill", "", "");
		table1.add("Jimmy", "couches", "5");
		table1.add("Jimmy", "dishes", "166");
		table1.add("Jimmy", "cream cheese", "Of_course!" );
		table1.add("Sal", "couches", "300");
		table1.add("Sal", "cell-phones", "888,120,499");
		table1.add("Sal", "cream cheese", "I'm broke...too many phones!");
		table1.add("Joe-bert", "dishes", "200000000");
		table1.add("Teej", "cream cheese", "What?");
		table1.add("Bill", "couches", "No_furniture");
		table1.add("Bill", "cream cheese", "Yup.");
		table1.add("", "New-Column", "");
	}
//=============================================================================
	@After
	public void finish() {
		System.out.println("\n");
		for(int r=0; r<5; r++) {
			for(int i=0; i<COL_WIDTH; i++) {
				System.out.print(SEPARATOR);
			}
			System.out.println();
		}
		System.out.println("\n");
	}
//=============================================================================
	public void printHeader(String testName) {
		System.out.print(testName + " ");
		for(int i=0; i<(COL_WIDTH - (testName.length() + 1)); i++) {
			System.out.print(SEPARATOR);
		}
		System.out.println();
		for(int r=0; r<4; r++) {
			for(int i=0; i<COL_WIDTH; i++) {
				System.out.print(SEPARATOR);
			}
			System.out.println();
		}
		System.out.println("\n");
	}
//=============================================================================
	public void indentOut(PrintStream outs, String data, char println) {
		String indent = "    ";
		if(println == 'y') {
			outs.println(indent + data);
		}
		else if(println == 'n') {
			outs.print(indent + data);
		}
		else {
			System.out.println("ERROR : Print line? 'y' or 'n'");
		}
	}
//=============================================================================
// JUnit Test Cases -----------------------------------------------------------
//=============================================================================
	@Test
	public void testConstructor() {
		printHeader("testConstructor()");

		fail("Not Implemented!");
	}
//=============================================================================
	@Test
	public void testAdd() {
		printHeader("testAdd()");
		testTable.add("row1", "col1", "apple");
		testTable.add("row2", "col2", "orange");
		testTable.add("row3", "col3", "banana");
		testTable.add("row1", "col2", "soda");
		testTable.add("row4", "col1", "seltzer");
		testTable.add("row1", "col1", "NEW_apples");
		testTable.describe();
		table1.describe();
		
		fail("Not Implemented!");
	}
//=============================================================================
	@Test
	public void testDelete() {
		printHeader("testDelete()");
		
		fail("Not Implemented");
	}
//=============================================================================
	@Test
	public void testDisplay_leftAlign() {
		printHeader("testDisplay_leftAlign()");
		table1.setRowTitle("This is the title!");
		table1.display('l');
		
		fail("Not Implemented");
	}
//=============================================================================
	@Test
	public void testDisplay_rightAlign() {
		printHeader("testDisplay_rightAlign()");
		//table1.setRowTitle("This is the title!");
		table1.display('r');
			
		fail("Not Implemented");
	}
//=============================================================================
	@Test
	public void testDisplay_centerAlign() {
		printHeader("testDisplay_centerAlign()");
		//table1.setRowTitle("This is the title!");
		table1.setName("Table Name");
		table1.setDescription(desc);
		table1.display('c');

		fail("Not Implemented");
	}
//=============================================================================
	@Test
	public void testSave() throws FileNotFoundException, IOException {
		printHeader("testSave()");
		table1.setName("My Test Table");
		table1.setDescription(desc);
		table1.save(file1, 'c');
		
		fail("Not Implemented");
	}
//=============================================================================
	@Test
	public void testLoadFromCsv() throws IOException {
		printHeader("testLoadFromCsv()");
		testTable.loadFromCsv(file3);
		testTable.setName("Bogus Table Heading");
		testTable.setDescription("Some description!");
		testTable.display('c');
		testTable.save(file4, 'c');
		
		fail("Not Implemented");
	}
//=============================================================================
	@Test
	public void testSaveToCsv() throws IOException {
		printHeader("testSaveToCsv()");
		testTable.loadFromCsv(file3);
		testTable.saveToCsv(file5);
		
		fail("Not Implemented");
	}
//=============================================================================
	@Test
	public void testLoad() throws IOException {
		printHeader("testLoad()");
		testTable.load(file1);
		testTable.display('c');
		testTable.save(file6, 'l');
		
		fail("Not Implemented");
	}
//=============================================================================
	@Test
	public void testToString() throws IOException {
		printHeader("testToString()");
		System.out.print(table1.toString('c'));
	}
//=============================================================================
} // End of TestTable.java ----------------------------------------------------